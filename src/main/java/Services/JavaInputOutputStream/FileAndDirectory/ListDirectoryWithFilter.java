package Services.JavaInputOutputStream.FileAndDirectory;

import java.io.File;
import java.io.FilenameFilter;

public class ListDirectoryWithFilter {
    public static void main(String[] args) {
        File dir = new File("D:\\Services\\WorkingOn\\java_core");
        if(dir.isDirectory()) {
            String[] files = dir.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String file) {
                    return file.endsWith(".java");
                }
            });
            for (String file : files) {
                System.out.println(file);
            }
        }
    }
}
