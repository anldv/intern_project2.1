package Services.JavaInputOutputStream.FileAndDirectory;

import java.io.File;

public class ListRecursive {
    public static void main(String[] args) {
        File dir = new File("d:\\");
        listRecursive(dir);
    }
     public static void listRecursive(File dir) {
        if (dir.isDirectory()) {
            File[] items = dir.listFiles();
            for(File item : items) {
                System.out.println(item.getAbsoluteFile());
                if (item.isDirectory()) {
                    listRecursive(item);
                }
            }
        }
     }
}
