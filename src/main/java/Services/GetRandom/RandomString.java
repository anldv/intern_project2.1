package Services.GetRandom;

import java.util.Random;

public class RandomString {

    public RandomString(){};

    public String getRandom(){
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234657890".toCharArray();
        StringBuilder sb = new StringBuilder(20);
        Random random1 = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random1.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        RandomString randomString = new RandomString();
        System.out.println(randomString.getRandom());
    }
}
