package Services.GetRandom;

import java.sql.*;
import java.util.Random;

public class RandomLocation {

    public RandomLocation() {
    }

    ;

    public String getRandomLocation() {
        String randomLocation = "";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/market?characterEncoding=utf8", "guess", "guess123password");
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM market.administrative_decentralized ORDER BY RAND() LIMIT 1;";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                randomLocation = "'" + resultSet.getString(2) + "', '" + resultSet.getString(4) + "', '" + resultSet.getString(6) + "'";
            }
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Error, see output console: " + e);
        }
        return randomLocation;
    }

    public static void main(String[] args) {
        RandomLocation randomLocation = new RandomLocation();
        String abc = randomLocation.getRandomLocation();
        System.out.println(abc);
        System.out.println(abc.replaceAll("[', ]", ""));
    }
}