package Services.GetRandom;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RandomTime {

    public RandomTime(){}

    public String getRandomTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        return format.format(date);
    }

    public static void main(String[] args) {
        RandomTime randomTime = new RandomTime();
        System.out.println(randomTime.getRandomTime());
    }
}
