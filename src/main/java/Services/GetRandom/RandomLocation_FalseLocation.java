package Services.GetRandom;

import java.sql.*;

public class RandomLocation_FalseLocation {
    public RandomLocation_FalseLocation() {
    }

    ;

    public String getRandomLocation_FalseLocation() {
        String randomLocation_false = "";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/market?characterEncoding=utf8", "guess", "guess123password");
            Statement statement = connection.createStatement();
            String sql1 = "SELECT province_cityId FROM market.administrative_decentralized ORDER BY RAND() LIMIT 1;";
            String sql2 = "SELECT district_townId FROM market.administrative_decentralized ORDER BY RAND() LIMIT 1;";
            String sql3 = "SELECT commune_wardId FROM market.administrative_decentralized ORDER BY RAND() LIMIT 1;";
            ResultSet resultSet1 = statement.executeQuery(sql1);
            String province_cityId = "";
            String district_townId = "";
            String commune_wardId = "";
            while (resultSet1.next()) {
                province_cityId = resultSet1.getString(1);
           }
            ResultSet resultSet2 = statement.executeQuery(sql2);
            while (resultSet2.next()) {
                district_townId = resultSet2.getString(1);
            }
            ResultSet resultSet3 = statement.executeQuery(sql3);
            while (resultSet3.next()) {
                commune_wardId = resultSet3.getString(1);
            }
            randomLocation_false = "'" + province_cityId + "', '" + district_townId + "', '" + commune_wardId + "'";
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Error, see output console: " + e);
        }
        return randomLocation_false;
    }

    public static void main(String[] args) {
        RandomLocation_FalseLocation randomLocation_falseLocation = new RandomLocation_FalseLocation();
        String abc = randomLocation_falseLocation.getRandomLocation_FalseLocation();
        System.out.println(abc);
        System.out.println(abc.replaceAll("[', ]", ""));
    }
}
