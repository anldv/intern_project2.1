package Services.GetRandom;

import java.sql.*;
import java.util.Random;

public class RandomReligion {

    public RandomReligion(){};

    public int getRandomReligion(){
        int randomReligion = 0;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/market?characterEncoding=utf8", "guess", "guess123password");
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM market.religion ORDER BY RAND() LIMIT 1;";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                randomReligion = resultSet.getInt(1);
            }
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Error, see output console: " + e);
        }
        return randomReligion;
    }

    public static void main(String[] args) {
        RandomReligion randomReligion = new RandomReligion();
        System.out.println(randomReligion.getRandomReligion());
    }
}
