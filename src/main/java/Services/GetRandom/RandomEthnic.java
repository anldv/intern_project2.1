package Services.GetRandom;

import java.sql.*;

public class RandomEthnic {

    public RandomEthnic(){};

    public int getRandomEthnic() {
        int randomEthnic = 0;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/market?characterEncoding=utf8", "guess", "guess123password");
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM market.ethnic ORDER BY RAND() LIMIT 1;";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                randomEthnic = resultSet.getInt(1);
            }
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Error, see output console: " + e);
        }
        return randomEthnic;
    }

    public static void main(String[] args) {
        RandomEthnic randomEthnic = new RandomEthnic();
        System.out.println(randomEthnic.getRandomEthnic());
    }
}
