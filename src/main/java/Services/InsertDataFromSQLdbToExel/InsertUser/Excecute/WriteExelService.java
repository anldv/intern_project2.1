package Services.InsertDataFromSQLdbToExel.InsertUser.Excecute;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.*;
import java.util.List;

public class WriteExelService extends Throwable {
    public void writeExcel(List<User> listUser, String excelFilePath) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        int i = sheet.getPhysicalNumberOfRows();
        System.out.println("Last row: " + i);
        int rowCount = 0;
        for (User aBook : listUser) {
            Row row = sheet.createRow(++rowCount);
            writeBook(aBook, row);
            i++;
            System.out.println("Write to exel file user:" + i);
        }

        try (FileOutputStream outputStream = new FileOutputStream(excelFilePath);
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream)) {
            workbook.write(bufferedOutputStream);
        }
    }



    public synchronized void writeError(String error, int rowNumber) {
        try {
            File file = new File("D:\\Services\\WorkingOn\\Edsolabs\\Report.1\\test1.xls");
            FileInputStream inputStream = new FileInputStream(file);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFSheet sheet = workbook.getSheetAt(0);
            HSSFCell cell = sheet.getRow(rowNumber).createCell(9);
            cell.setCellValue(error);
            inputStream.close();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            workbook.write(fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            System.out.println("Error" + e);
        }
    }

    private void writeBook(User anUser, Row row) {
        Cell cell = row.createCell(1);
        cell.setCellValue(anUser.getUserName());

        cell = row.createCell(2);
        cell.setCellValue(anUser.getGenderId());

        cell = row.createCell(3);
        cell.setCellValue(anUser.getProvince_cityId());

        cell = row.createCell(4);
        cell.setCellValue(anUser.getDistrict_townId());

        cell = row.createCell(5);
        cell.setCellValue(anUser.getCommune_wardId());

        cell = row.createCell(6);
        cell.setCellValue(anUser.getReligionId());

        cell = row.createCell(7);
        cell.setCellValue(anUser.getEthnicId());

        cell = row.createCell(8);
        cell.setCellValue(anUser.getMd5hash());
    }
}
