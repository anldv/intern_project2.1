package Services.InsertDataFromSQLdbToExel.InsertUser.Excecute.SubService.Validate;

import Services.InsertDataFromSQLdbToExel.InsertUser.Excecute.*;

import java.util.ArrayList;
import java.util.List;

public class ValidateExelFile extends Thread {
    public static int i = 1;
    private int from;
    private int to;
    private int threadNumber;
    public static long start;
    public static long finish;
    public List<String> errorList = new ArrayList<>();

    public ValidateExelFile(int from, int to, int threadNumber) {
        this.from = from;
        this.to = to;
        this.threadNumber = threadNumber;
        start();
    }

    ReadCellData readCellData = new ReadCellData();
    ValidateUser validateUser = new ValidateUser();
    WriteExelService writeExelService = new WriteExelService();

//    public void getAndCheck() {
//        if (i == 10002) {
//            finish = System.currentTimeMillis();
//            System.out.println("Running time: " + (finish - start));
//            return;
//        } else {
//            String error = "";
//            error = error + validateUser.validateUser(readCellData.ReadCellData(i));
//            if (error.equals("")) {
//                System.out.println("OK");
//            } else System.out.println(error);
//            i++;
//        }
//    }

    public void getAndCheck2(int from, int to, int threadNumber) {
        int count = from;
        while (count < to) {
            if (count == 1) {
                start = System.currentTimeMillis();
            }
            System.out.println("Thread " + threadNumber + " is running...");
            String error = "";
            error = error + validateUser.validateUser(readCellData.ReadCellData(count));
            if (error.equals("")) {
                System.out.println("OK");
                errorList.add("OK");
            } else {
                System.out.println(error);
                errorList.add(error);
            }
            count++;
            if (count == to) {
                finish = System.currentTimeMillis();
                System.out.println("Start time: " + start + "mS");
                System.out.println("Finish time: " + finish + "mS");
                System.out.println("Run time: " + (finish - start) + "mS");
                writeErrorToExcel();
            }
        }
    }

    public synchronized void writeErrorToExcel() {
        WriteExelService writeExelService = new WriteExelService();
        for(int i=from; i<to; i++) {
            writeExelService.writeError(errorList.get(i), (i+1));
        }
    }

    @Override
    public void run() {
        getAndCheck2(this.from, this.to, this.threadNumber);
    }

    public static void main(String[] args) {
        ValidateExelFile thread1 = new ValidateExelFile(1, 166, 1);
        ValidateExelFile thread2 = new ValidateExelFile(167, 332, 2);
        ValidateExelFile thread3 = new ValidateExelFile(333, 498, 3);
        ValidateExelFile thread4 = new ValidateExelFile(499, 664, 4);
        ValidateExelFile thread5 = new ValidateExelFile(665, 830, 5);
        ValidateExelFile thread6 = new ValidateExelFile(831, 1001, 6);
//        ValidateExelFile thread7 = new ValidateExelFile(751, 875, 7);
//        ValidateExelFile thread8 = new ValidateExelFile(876, 1001, 8);
//        ValidateExelFile thread9 = new ValidateExelFile(801, 900, 9);
//        ValidateExelFile thread10 = new ValidateExelFile(901, 1001, 10);

    }
}
// 10 thread
//Start time: 1623377644574mS
//Finish time: 1623377684289mS
//Run time: 39715mS

// 8 thread
//Start time: 1623377781151mS
//Finish time: 1623377819438mS
//Run time: 38287mS

// 6 thread
//Start time: 1623377941362mS
//Finish time: 1623377976516mS
//Run time: 35154mS

// 5 thread
//Start time: 1623377494456mS
//Finish time: 1623377526739mS
//Run time: 32283mS

// 4 thread
//Start time: 1623377417517mS
//Finish time: 1623377452258mS
//Run time: 34741mS

// 2 thread
//Start time: 1623377352586mS
//Finish time: 1623377398793mS
//Run time: 46207mS

// 1 thread
//Start time: 1623377234613mS
//Finish time: 1623377312937mS
//Run time: 78324mS