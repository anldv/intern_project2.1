package Services.InsertDataFromSQLdbToExel.InsertUser.Excecute.SubService.Validate;

import Services.InsertDataFromSQLdbToExel.InsertUser.Excecute.*;

import java.sql.*;

public class ValidateLocation {
    public ValidateLocation() {}

    public String validateUserLocation(User inputUser) {
        String error = "";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/market?characterEncoding=utf8", "guess", "guess123password");
            Statement statement = connection.createStatement();
            String sql = "SELECT distinct * FROM market.administrative_decentralized where commune_wardId = '" + inputUser.getCommune_wardId() + "';";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                int commune_wardId_db = resultSet.getInt(6);
                String commune_wardName_db = resultSet.getString(5);
                int district_townId_db = resultSet.getInt(4);
                String district_townName_db = resultSet.getString(3);
                int province_cityId_db = resultSet.getInt(2);
                String province_cityName_db = resultSet.getString(1);
                if (district_townId_db != inputUser.getDistrict_townId()) {
                    error = error + ("Lỗi địa chỉ: " + commune_wardName_db + " phải thuộc " + district_townName_db + ". ");
                }
                if (province_cityId_db != inputUser.getProvince_cityId()) {
                    error = error + ("Lỗi địa chỉ: " + district_townName_db + " phải thuộc " + province_cityName_db + ". ");
                }
            }
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Error, see output console: " + e);
        }
        return error;
    }
}
