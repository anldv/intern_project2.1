package Services.InsertDataFromSQLdbToExel.InsertUser.Excecute.SubService.Validate;

import Services.InsertDataFromSQLdbToExel.InsertUser.Excecute.User;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadCellData {

    public ReadCellData(){};

    public User ReadCellData(int vRow)
    {
        String value = null;          //variable for storing the cell value
        Workbook wb = null;           //initialize Workbook null
        try
        {
            //reading data from a file in the form of bytes
            FileInputStream fis = new FileInputStream("D:\\Services\\WorkingOn\\Edsolabs\\Report.1\\test1.xls");
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fis);
            //constructs an XSSFWorkbook object, by buffering the whole stream into the memory
            wb = new HSSFWorkbook(bufferedInputStream);
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch(IOException e1)
        {
            e1.printStackTrace();
        }
        Sheet sheet = wb.getSheetAt(0);   //getting the XSSFSheet object at given index
        Row row=sheet.getRow(vRow); //returns the logical row
        String userName = row.getCell(1).getStringCellValue();
        int genderId = (int) row.getCell(2).getNumericCellValue();
        int province_cityId = (int) row.getCell(3).getNumericCellValue();
        int district_townId = (int) row.getCell(4).getNumericCellValue();
        int commune_wardId = (int) row.getCell(5).getNumericCellValue();
        int religionId = (int) row.getCell(6).getNumericCellValue();
        int ethnicId = (int) row.getCell(7).getNumericCellValue();
        String md5hash = row.getCell(8).getStringCellValue();
        User thisUser = new User(userName, genderId, province_cityId, district_townId, commune_wardId, religionId, ethnicId, md5hash);

//        value=cell.getStringCellValue();    //getting cell value
        return thisUser;               //returns the cell value
    }

    public static void main(String[] args) {

        ReadCellData readCellData = new ReadCellData();
        ValidateLocation validateLocation = new ValidateLocation();

        for (int i=1; i<10002; i++) {
            String error = "" + validateLocation.validateUserLocation(readCellData.ReadCellData(i));
            if (error.equals("")) {
                System.out.println("OK");
            } else {
                System.out.println(error);
            }
        }

    }
}
