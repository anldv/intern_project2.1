package Services.InsertDataFromSQLdbToExel.InsertUser.Excecute.SubService.WriteToDb;

import Services.InsertDataFromSQLdbToExel.InsertUser.Excecute.User;

import java.sql.*;
import java.util.List;

public class WriteDbService {

    public WriteDbService(){}

    public void writeDb (List<User> list) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/market?characterEncoding=utf8", "guess", "guess123password");
            Statement statement = connection.createStatement();
            String sql = "";

            for (int i=0; i<list.size(); i++) {
                System.out.println("Write to Db user: " + (i+1));
                sql = "insert into market.user_db2 (userName, genderId, province_cityId, district_townId, commune_wardId, religionId, ethnicId, md5hash) " +
                        "values ('"+list.get(i).getUserName()+"', "+list.get(i).getGenderId()+", "+list.get(i).getProvince_cityId()+", "+list.get(i).getDistrict_townId()+", "+list.get(i).getCommune_wardId()+", "+list.get(i).getEthnicId()+", "+list.get(i).getReligionId()+", '"+list.get(i).getMd5hash()+"');";
                statement.execute(sql);
            }

            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Error, see output console: " + e);
        }
    }
}
