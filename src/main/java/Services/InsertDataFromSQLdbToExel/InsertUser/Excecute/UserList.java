package Services.InsertDataFromSQLdbToExel.InsertUser.Excecute;

import Services.Encrypt.MD5.*;
import Services.GetRandom.*;

import java.util.ArrayList;
import java.util.List;

public class UserList {

    RandomString randomString = new RandomString();
    RandomGender randomGender = new RandomGender();
    RandomLocation randomLocation = new RandomLocation();
    RandomLocation_FalseLocation randomLocation_falseLocation = new RandomLocation_FalseLocation();
    RandomReligion randomReligion = new RandomReligion();
    RandomEthnic randomEthnic = new RandomEthnic();
    MD5 md5service = new MD5();

    public User getRandomUser() {
        String userName = randomString.getRandom();
        int gender = randomGender.getRandomGender();
        int religion = randomReligion.getRandomReligion();
        int ethnic = randomEthnic.getRandomEthnic();
        String location = "";

        int random = (int) ((Math.random() * (3 - 1)) + 1);
        if (random == 1) {
            location = randomLocation.getRandomLocation();
        } else {
            location = randomLocation_falseLocation.getRandomLocation_FalseLocation();
        }

        //String location to subString array location include province, district,...
        String[] location1 = location.replaceAll("[',]", "").split(" ");
        int[] location2 = new int[3];
        for(int j=0; j<3; j++) {
            location2[j] = Integer.valueOf(location1[j]);
        }
        int province_city = location2[0];
        int district_town = location2[1];
        int commune_ward = location2[2];
        String hashCode = md5service.getMD5Hash(userName+gender+province_city+district_town+commune_ward+religion+ethnic);
        User thisUser = new User(userName, gender, province_city, district_town, commune_ward, religion, ethnic, hashCode);
        return thisUser;
    }

    public List<User> getRandomUserList(int numberOfUser)
    {
        List<User> list = new ArrayList<>();
            for(int i=0; i<numberOfUser; i++) {
                System.out.println("Insert user:" + (i+1));
                list.add(getRandomUser());
            }
        return list;
    }

    public static void main(String[] args) {

    }
}
