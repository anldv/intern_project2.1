package Services.InsertDataFromSQLdbToExel.InsertUser.Excecute;

import Services.InsertDataFromSQLdbToExel.InsertUser.Excecute.SubService.WriteToDb.WriteDbService;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class main {
    public static void main(String[] args) throws IOException {
        UserList userList = new UserList();
        File file = new File("D:\\Services\\WorkingOn\\Edsolabs\\Report.1\\test1.xls");
        WriteExelService writeExelService = new WriteExelService();
        WriteDbService writeDbService = new WriteDbService();
        //ValidateExelFile validateExelFile = new ValidateExelFile();
        //Scanner scanner = new Scanner(System.in);
        List<User> list = userList.getRandomUserList(100);
        //Write user to Db
        //...
        writeDbService.writeDb(list);

        //Write user to xls
        writeExelService.writeExcel(list, "D:\\Services\\WorkingOn\\Edsolabs\\Report.1\\test1.xls");
    }
}
