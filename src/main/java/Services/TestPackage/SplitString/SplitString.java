package Services.TestPackage.SplitString;

import java.util.ArrayList;
import java.util.List;

public class SplitString {
    public ArrayList<String> splitStringEvery(String s, int interval) {
        ArrayList<String> list = new ArrayList<>();

        int arrayLength = (int) Math.ceil(((s.length() / (double)interval)));
        String[] results = new String[arrayLength];

        int j = 0;
        int lastIndex = results.length - 1;
        for (int i = 0; i < lastIndex; i++) {
            results[i] = s.substring(j, j + interval);
            j += interval;
        } //Add the last bit
        results[lastIndex] = s.substring(j);

        for (int i = 0; i < results.length; i++) {
            list.add(results[i]);
        }
        return list;
    }

    public static void main(String[] args) {
        String sample = "134252rfsgvaszdfvsvdfvdgfsdfgvdf";
        SplitString splitString = new SplitString();
        ArrayList<String> list = splitString.splitStringEvery(sample, 3);
        for (String element : list) {
            System.out.println(element);
        }
    }
}
