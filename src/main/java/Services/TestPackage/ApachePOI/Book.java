package Services.TestPackage.ApachePOI;

public class Book {
    private int id;
    private String title;
    private double price;
    private int quantity;
    private double totalMoney;

    public Book() {
    }

    public Book(int id, String title, double price, int quantity, double totalMoney) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.quantity = quantity;
        this.totalMoney = totalMoney;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(double totalMoney) {
        this.totalMoney = totalMoney;
    }
}
