package Services.Stream.Example;

import java.io.*;

public class Example1 {

    public static void main(String[] args) throws IOException{
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;

        try {
            fileInputStream = new FileInputStream("D:\\Services\\WorkingOn\\Edsolabs\\Report.1\\input.txt");
            fileOutputStream = new FileOutputStream("D:\\Services\\WorkingOn\\Edsolabs\\Report.1\\output.txt");

            int c;
             while((c = fileInputStream.read()) != -1) {
                 fileOutputStream.write(c);
             }
        } finally {
            if(fileInputStream != null) {
                fileInputStream.close();
            } if(fileOutputStream != null) {
                fileOutputStream.close();
            }
        }
    }
}
