package Services.Stream.Example;

import java.io.*;

public class Example3 {

    public static void main(String[] args) {
        try {
            byte bWrite[] = {11, 21, 3, 40, 5};
            OutputStream outputStream = new FileOutputStream("D:\\Services\\WorkingOn\\Edsolabs\\Report.1\\test1.txt");
            for(int x=0; x<bWrite.length; x++) {
                outputStream.write((int)bWrite[x]);
            }
            outputStream.close();

            InputStream inputStream = new FileInputStream("D:\\Services\\WorkingOn\\Edsolabs\\Report.1\\test1.txt");

            int size = inputStream.available();

            for(int i=0; i<size; i++) {
                System.out.print((int)inputStream.read() + " ");
            }

            inputStream.close();
        } catch (IOException e) {
            System.out.println("Exception: " + e);
        }
    }
}
