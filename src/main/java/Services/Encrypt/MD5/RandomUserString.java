package Services.Encrypt.MD5;

import Services.GetRandom.*;

public class RandomUserString {

    public RandomUserString(){}

    RandomLocation randomLocation = new RandomLocation();
    RandomReligion randomReligion = new RandomReligion();
    RandomEthnic randomEthnic = new RandomEthnic();
    RandomString randomString = new RandomString();
    RandomGender randomGender = new RandomGender();

    public String getRandomUSerString() {
        String name = randomString.getRandom();
        int gender = randomGender.getRandomGender();
        String location = randomLocation.getRandomLocation().replaceAll("[', ]","");
        int religion = randomReligion.getRandomReligion();
        int ethnic = randomEthnic.getRandomEthnic();
        return name + gender + location + religion + ethnic;
    }
}
