package Services.InsertDataFromExelToSQLdb.InsertUser;

import Services.Encrypt.MD5.MD5;
import Services.GetRandom.*;

import java.sql.*;

public class InsertUser {

    RandomLocation randomLocation = new RandomLocation();
    RandomReligion randomReligion = new RandomReligion();
    RandomEthnic randomEthnic = new RandomEthnic();
    RandomString randomString = new RandomString();
    RandomTime randomTime = new RandomTime();
    RandomGender randomGender = new RandomGender();
    MD5 md5service = new MD5();

    public InsertUser(int numberOfUser){
        int i = 0;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/market?characterEncoding=utf8", "guess", "guess123password");
            Statement statement = connection.createStatement();
            while (i<numberOfUser) {
                String randomUserName = randomString.getRandom();
                String randomUserPassword = randomString.getRandom();
                int randomUserReligion = randomReligion.getRandomReligion();
                int randomUserEthnic = randomEthnic.getRandomEthnic();
                int randomUserGender = randomGender.getRandomGender();
                String randomUserLocation = randomLocation.getRandomLocation();
                String randomUserTime = randomTime.getRandomTime();
                String md5builder = randomUserName+randomUserPassword+randomUserReligion+randomUserEthnic+randomUserGender+randomUserLocation+randomUserTime;
                String md5input = md5builder.replaceAll("[', ]", "");

                String sql = "insert into market.user_db (userName, userPassword, genderId, province_cityId, district_townId, commune_wardId, ethnicId, religionId, user_db.date, updateTime, md5hash) values " +
                        "('"+ randomUserName + "', '" + randomUserPassword + "', '" + randomUserGender + "', " + randomUserLocation + ", '" +
                        randomUserEthnic +"', '" + randomUserReligion + "', '" + randomUserTime + "', '"+ randomUserTime + "', '" + md5service.getMD5Hash(md5input) + "');";
                System.out.println(sql);
                statement.execute(sql);
                i++;
                System.out.println("Insert user number: " + i + " success!");
            }
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Error, see output console: " + e);
        }
    };

    public static void main(String[] args) {
        InsertUser insertUser = new InsertUser(5000000);
    }
}
