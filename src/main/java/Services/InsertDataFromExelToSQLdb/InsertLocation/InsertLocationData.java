package Services.InsertDataFromExelToSQLdb.InsertLocation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertLocationData {
    public static void main(String[] args) {
        ReadCellData readCellData = new ReadCellData();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/market?characterEncoding=utf8", "guess", "guess123password");
            Statement statement = connection.createStatement();
            for(int i=1; i<16019; i++){
                System.out.println();

                String province_cityName = readCellData.ReadCellData(i, 0);
                String province_cityId = readCellData.ReadCellData(i, 1);
                String district_townName = readCellData.ReadCellData(i, 2);
                String district_townId = readCellData.ReadCellData(i, 3);
                String commune_wardName = readCellData.ReadCellData(i, 4);
                String commune_wardId = readCellData.ReadCellData(i, 5);

                String sql = "insert into market.administrative_decentralized (province_cityName, province_cityId, district_townName, district_townId, commune_wardName, commune_wardId) values "
                        + "('" + province_cityName + "', '" + province_cityId + "', '" + district_townName + "', '" + district_townId + "', '" + commune_wardName + "', '" + commune_wardId + "');";

                System.out.println("Generate sql success: " + sql);
                statement.execute(sql);
                System.out.println("Insert data success!");
            }
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Error, see output console: " + e);
        }
    }
}
