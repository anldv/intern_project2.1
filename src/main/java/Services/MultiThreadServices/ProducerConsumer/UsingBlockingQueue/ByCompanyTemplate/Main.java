package Services.MultiThreadServices.ProducerConsumer.UsingBlockingQueue.ByCompanyTemplate;

import Services.MultiThreadServices.ProducerConsumer.UsingBlockingQueue.ByCompanyTemplate.CdrExecuteManager;

public class Main {

    public static void main(String[] args) {
        CdrExecuteManager manager = CdrExecuteManager.getInstance();
        manager.start();
    }
}
