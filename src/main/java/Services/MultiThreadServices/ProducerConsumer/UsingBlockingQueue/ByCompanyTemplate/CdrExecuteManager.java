package Services.MultiThreadServices.ProducerConsumer.UsingBlockingQueue.ByCompanyTemplate;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class CdrExecuteManager {

    private static CdrExecuteManager ourInstance = new CdrExecuteManager();

    private final BlockingQueue<String> queue;

    private CdrExecuteManager() {
        queue = new LinkedBlockingDeque<>();
        init();
    }

    public static CdrExecuteManager getInstance() {
        return ourInstance;
    }

    private int random(){
        return new Random().nextInt(1000);
    }

    public void start() {
        while (true){
            int number = random();
            put(String.valueOf(number));
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void init() {
        System.out.println("CdrExecuteManager.init()");
//        int numWorker = AppConfig.getConfig().getIntProperty("WORKER_CDR", 50, "SETTINGS");
        int numWorker = 10;
        String name;
        for (int i = 0; i < numWorker; i++) {
            name = String.format("CdrExecuteWorker %d", (i + 1));
            CdrExecuteWorker worker = new CdrExecuteWorker(name, queue);
            worker.execute();
        }
    }

    public void put(String msg) {
        queue.offer(msg);
    }
}
