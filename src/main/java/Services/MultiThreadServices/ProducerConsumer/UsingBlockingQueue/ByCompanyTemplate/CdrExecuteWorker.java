package Services.MultiThreadServices.ProducerConsumer.UsingBlockingQueue.ByCompanyTemplate;

import java.util.concurrent.BlockingQueue;

public class CdrExecuteWorker extends ThreadBase {

    private final BlockingQueue<String> queue;

    public CdrExecuteWorker(String name, BlockingQueue<String> queue) {
        super(name);
        this.queue = queue;
    }

    @Override
    protected void onExecuting() throws Exception {
        System.out.println("Thread " + getName() + " started");
    }

    @Override
    protected void onKilling() {
        this.kill();
        System.out.println("Thread " + getName() + " killed");
    }

    @Override
    protected void onException(Throwable th) {
        System.err.println(th.getMessage());
    }

    @Override
    protected long sleepTime() throws Exception {
        return 100;
    }

    @Override
    protected void action() throws Exception {
        String str = queue.poll();
        if(str == null){
            return;
        }
        System.out.println(this.getName() + " : " + str);
    }
}
