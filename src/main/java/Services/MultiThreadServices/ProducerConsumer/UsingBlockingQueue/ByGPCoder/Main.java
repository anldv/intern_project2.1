package Services.MultiThreadServices.ProducerConsumer.UsingBlockingQueue.ByGPCoder;

public class Main {
    public static void main(String[] args) {
        try {
            BlockingQueue<String> queue = new BlockingQueue<>();
            Producer producer = new Producer(queue);
            Consumer consumer1 = new Consumer(queue);
            Consumer consumer2 = new Consumer(queue);
            Consumer consumer3 = new Consumer(queue);

            new Thread(producer).start();
            new Thread(consumer1).start();
            new Thread(consumer2).start();

            Thread.sleep(5000);

            new Thread(consumer3).start();
        } catch (InterruptedException e) {
            System.out.println("Error" + e);
        }
    }
}
