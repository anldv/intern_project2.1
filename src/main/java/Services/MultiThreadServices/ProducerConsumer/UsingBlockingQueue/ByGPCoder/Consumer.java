package Services.MultiThreadServices.ProducerConsumer.UsingBlockingQueue.ByGPCoder;

import org.apache.poi.ss.formula.functions.T;

import java.util.concurrent.ThreadLocalRandom;

public class Consumer implements Runnable{
    private final BlockingQueue<String> queue;

    public Consumer(BlockingQueue<String> queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            while (true) {
                String message = queue.take();
                System.out.println("Consumer consume: " + message);
                System.out.println("Consumer resource - Queue size = " + queue.size());
                Thread.sleep(ThreadLocalRandom.current().nextInt(50, 300));
            }
        } catch (InterruptedException e) {
            System.out.println("Error" + e);
        }
    }
}
