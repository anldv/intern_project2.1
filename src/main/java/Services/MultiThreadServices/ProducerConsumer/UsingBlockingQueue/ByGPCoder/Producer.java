package Services.MultiThreadServices.ProducerConsumer.UsingBlockingQueue.ByGPCoder;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Producer implements Runnable{
    private final BlockingQueue<String> queue;

    public Producer(BlockingQueue<String> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String message = getRandomMessage();
                System.out.println("Producer produce message: " + message);
                queue.put(message);
                System.out.println("Producer resource - Queue size = " + queue.size());
            }
        } catch (InterruptedException e) {
            System.out.println("Error: " + e);
        }
    }

    private int produce() throws InterruptedException {
        Thread.sleep(50);
        return ThreadLocalRandom.current().nextInt(1, 100);
    }

    public String getRandomMessage() {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234657890".toCharArray();
        StringBuilder sb = new StringBuilder(20);
        Random random1 = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random1.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }
}
