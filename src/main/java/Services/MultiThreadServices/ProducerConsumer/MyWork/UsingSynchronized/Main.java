package Services.MultiThreadServices.ProducerConsumer.MyWork.UsingSynchronized;

import java.util.LinkedList;
import java.util.Queue;

public class Main {
    public static void main(String[] args) {
        Producer producer = new Producer();
        Consumer consumer = new Consumer(producer);
        producer.start();
        consumer.start();
    }
}
