package Services.MultiThreadServices.ProducerConsumer.MyWork.UsingSynchronized;

public class Consumer extends Thread{
    private Producer producer;

    public Consumer(Producer producer) {
        this.producer = producer;
    }

    @Override
    public void run() {
        try {
            while (true) {
                System.out.println("Consumer consume message: " + producer.getMessage());
                sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Error: " + e);
        }
    }
}
