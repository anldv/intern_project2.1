package Services.MultiThreadServices.ProducerConsumer.MyWork.UsingSynchronized;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class Producer extends Thread{
    private Queue<String> queue = new LinkedList<>();
    private final int queueSize = 10;

    @Override
    public void run() {
        while (true) {
            putMessage();
            try {
                sleep(500);
            } catch (InterruptedException e) {
                System.out.println("Error: " + e);
            }
        }
    }

    public synchronized void putMessage() {
        try {
            while (queue.size() == queueSize) {
                wait();
            }
            String message = getRandomMessage();
            System.out.println("Producer produce message: " + message);
            queue.offer(message);
            notify();
        } catch (InterruptedException e) {
            System.out.println("Error: " + e);
        }
    }

    public synchronized String getMessage() {
        String message = "";
        try {
            notify();
            while (queue.size() == 0) {
                wait();
            }
            message = queue.poll();
        } catch (InterruptedException e) {
            System.out.println("Error: " + e);
        }
        return message;
    }

    public String getRandomMessage(){
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234657890".toCharArray();
        StringBuilder sb = new StringBuilder(20);
        Random random1 = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random1.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }
}
