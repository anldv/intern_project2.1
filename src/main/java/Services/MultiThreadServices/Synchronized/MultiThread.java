package Services.MultiThreadServices.Synchronized;

public class MultiThread {
    private int i = 0;

    public void increment() {
        i++;
    }

    public int getValue() {
        return i;
    }
}

class main {
    public static void main(String[] args) {
        MultiThread thread = new MultiThread();
        thread.increment();
        System.out.println(thread.getValue());
    }
}
