package Services.MultiThreadServices.Synchronized;

import java.util.ArrayList;
import java.util.List;

public class AClassForThreadExample {
    String name = "";
    public int count = 0;

    public void threadName(String name, List<String> list) {
        synchronized (this) {
            this.name = name;
            count++;
        }
        list.add(name);
    }
}

class anotherNewMain {
    public static void main(String[] args) {
        AClassForThreadExample aClassForThreadExample = new AClassForThreadExample();
        List<String> list = new ArrayList<>();
        aClassForThreadExample.threadName("Nah", list);
        System.out.println(aClassForThreadExample.name);
    }
}
