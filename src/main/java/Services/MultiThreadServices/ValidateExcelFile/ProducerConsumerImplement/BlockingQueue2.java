package Services.MultiThreadServices.ValidateExcelFile.ProducerConsumerImplement;

import java.util.LinkedList;

public class BlockingQueue2<ErrorEntity> {
    private static final int capacity = 50;
    private final LinkedList<ErrorEntity> queue2 = new LinkedList<>();

    public synchronized void put2(ErrorEntity errorEntity) {
        try {
            while (queue2.size() == capacity) {
                System.out.println("Queue2 is full!");
                wait();
            }
            System.out.println("Put to queue2 success!");
            queue2.addLast(errorEntity);
            notify();
        } catch (InterruptedException e) {
            System.out.println("Error " + e);
        }
    }

    public synchronized ErrorEntity take2() {
        ErrorEntity errorEntity = null;
        try {
            while (queue2.size() == 0) {
                System.out.println("Queue2 is empty");
                wait();
            }
            System.out.println("Take from queue2");
            notify();
            errorEntity = queue2.removeFirst();
        } catch (InterruptedException e) {
            System.out.println("Error: " + e);
        }
        return errorEntity;
    }

    public synchronized int size2() {
        return queue2.size();
    }
}
