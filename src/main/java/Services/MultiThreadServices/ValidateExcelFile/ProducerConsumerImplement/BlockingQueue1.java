package Services.MultiThreadServices.ValidateExcelFile.ProducerConsumerImplement;

import java.util.LinkedList;

public class BlockingQueue1<UserEntity> {
    private static final int capacity = 50;
    private final LinkedList<UserEntity> queue = new LinkedList<>();

    public synchronized void put(UserEntity user) {
        try {
            while (queue.size() == capacity) {
                System.out.println("Queue1 is full!");
                wait();
            }
            System.out.println("Put to queue1 success!");
            queue.addLast(user);
            notify();
        } catch (InterruptedException e) {
            System.out.println("Error: " + e);
        }
    }

    public synchronized UserEntity take() {
        UserEntity user = null;
        try {
            while (queue.size() == 0) {
                System.out.println("Queue1 is empty");
                wait();
            }
            System.out.println("Take to queue1 success!");
            user = queue.removeFirst();
            notify();
        } catch (InterruptedException e) {
            System.out.println("Error: " + e);
        }
        return user;
    }

    public synchronized int size() {
        return queue.size();
    }
}
