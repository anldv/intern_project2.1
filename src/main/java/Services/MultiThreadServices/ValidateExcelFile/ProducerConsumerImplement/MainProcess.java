package Services.MultiThreadServices.ValidateExcelFile.ProducerConsumerImplement;

import Services.MultiThreadServices.ValidateExcelFile.Entities.ErrorEntity;
import Services.MultiThreadServices.ValidateExcelFile.Entities.UserEntity;
import Services.MultiThreadServices.ValidateExcelFile.ExcelServices;
import Services.MultiThreadServices.ValidateExcelFile.WorkBookServices;

public class MainProcess {
    public static void main(String[] args) {
        WorkBookServices workBookServices = new WorkBookServices("D:\\Services\\WorkingOn\\Edsolabs\\Report.1\\test3.xls");
        ExcelServices excelServices = new ExcelServices(workBookServices);
        BlockingQueue1<UserEntity> queue1 = new BlockingQueue1<>();
        BlockingQueue2<ErrorEntity> queue2 = new BlockingQueue2<>();

        Producer1Read producer1Read = new Producer1Read(queue1, workBookServices, excelServices);
        Consumer1Producer2Validate consumer1Producer2Validate = new Consumer1Producer2Validate(queue1, queue2, workBookServices);
        Consumer1Producer2Validate consumer1Producer2Validate1 = new Consumer1Producer2Validate(queue1, queue2, workBookServices);
        Consumer1Producer2Validate consumer1Producer2Validate2 = new Consumer1Producer2Validate(queue1, queue2, workBookServices);
        Consumer1Producer2Validate consumer1Producer2Validate3 = new Consumer1Producer2Validate(queue1, queue2, workBookServices);
        Consumer1Producer2Validate consumer1Producer2Validate4 = new Consumer1Producer2Validate(queue1, queue2, workBookServices);
        Consumer2Write consumer2Write = new Consumer2Write(queue2, workBookServices);

        Thread t1 = new Thread(producer1Read);
        Thread t2 = new Thread(consumer1Producer2Validate);
        Thread t3 = new Thread(consumer1Producer2Validate1);
        Thread t4 = new Thread(consumer1Producer2Validate2);
        Thread t5 = new Thread(consumer1Producer2Validate3);
        Thread t6 = new Thread(consumer1Producer2Validate4);
        Thread t7 = new Thread(consumer2Write);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();

    }
}
