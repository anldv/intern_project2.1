package Services.MultiThreadServices.ValidateExcelFile.ProducerConsumerImplement;

import Services.MultiThreadServices.ValidateExcelFile.Entities.ErrorEntity;
import Services.MultiThreadServices.ValidateExcelFile.Entities.UserEntity;
import Services.MultiThreadServices.ValidateExcelFile.ExcelServices;
import Services.MultiThreadServices.ValidateExcelFile.WorkBookServices;

public class Consumer1Producer2Validate implements Runnable{
    private final BlockingQueue1<UserEntity> queue1;
    private final BlockingQueue2<ErrorEntity> queue2;
    private WorkBookServices workBookServices;
    private ExcelServices excelServices;

    public Consumer1Producer2Validate(BlockingQueue1<UserEntity> queue1, BlockingQueue2<ErrorEntity> queue2, WorkBookServices workBookServices) {
        this.excelServices = new ExcelServices(workBookServices);
        this.queue1 = queue1;
        this.queue2 = queue2;
    }

    @Override
    public void run() {
        while (true) {
            UserEntity userEntity = queue1.take();
            String error = excelServices.validateUserLocation(userEntity);
            ErrorEntity errorEntity = new ErrorEntity(userEntity.getUserName(), error);
            queue2.put2(errorEntity);
        }
    }
}
