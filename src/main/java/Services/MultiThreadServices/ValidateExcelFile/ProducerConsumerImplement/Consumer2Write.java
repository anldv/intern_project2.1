package Services.MultiThreadServices.ValidateExcelFile.ProducerConsumerImplement;

import Services.MultiThreadServices.ValidateExcelFile.Entities.ErrorEntity;
import Services.MultiThreadServices.ValidateExcelFile.ExcelServices;
import Services.MultiThreadServices.ValidateExcelFile.WorkBookServices;

public class Consumer2Write implements Runnable{
    private final BlockingQueue2<ErrorEntity> queue2;
    private WorkBookServices workBookServices;
    private ExcelServices excelServices;

    public Consumer2Write(BlockingQueue2<ErrorEntity> queue2, WorkBookServices workBookServices) {
        this.queue2 = queue2;
        this.excelServices = new ExcelServices(workBookServices);
        this.workBookServices = workBookServices;
    }

    @Override
    public void run() {
        while (true) {
            ErrorEntity errorEntity = queue2.take2();
            System.out.println("Consumer2 consume: " + errorEntity.getError());
            System.out.println("Consumer2 resource - Queue size = " + queue2.size2() );
            excelServices.writeError(errorEntity);
        }
    }
}
