package Services.MultiThreadServices.ValidateExcelFile.ProducerConsumerImplement;

import Services.MultiThreadServices.ValidateExcelFile.Entities.UserEntity;
import Services.MultiThreadServices.ValidateExcelFile.ExcelServices;
import Services.MultiThreadServices.ValidateExcelFile.WorkBookServices;
import org.apache.poi.ss.usermodel.Row;

public class Producer1Read implements Runnable{
    private final BlockingQueue1<UserEntity> queue;
    private WorkBookServices workBookServices;
    private ExcelServices excelServices;

    public Producer1Read(BlockingQueue1<UserEntity> queue1, WorkBookServices workBookServices, ExcelServices excelServices) {
        this.queue = queue1;
        this.excelServices = excelServices;
        this.workBookServices = workBookServices;
    }

    @Override
    public void run() {
        int i = 1;
        for (Row row : workBookServices.getWorkBookServices().getSheetAt(0)){
            UserEntity thisUserEntity = excelServices.readUserByRow(row);
            System.out.println("Produce user by row number: " + (i+1) + ". Name: " + thisUserEntity.getUserName());
            queue.put(thisUserEntity);
            System.out.println("Producer resource - Queue size = " + queue.size());
            i++;
        }
        System.out.println("Read finished!");
    }
}

