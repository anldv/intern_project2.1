package Services.MultiThreadServices.ValidateExcelFile.SemaphoreImplement;

import Services.MultiThreadServices.ValidateExcelFile.Entities.ErrorEntity;
import Services.MultiThreadServices.ValidateExcelFile.Entities.UserEntity;
import Services.MultiThreadServices.ValidateExcelFile.ExcelServices;
import Services.MultiThreadServices.ValidateExcelFile.WorkBookServices;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class ProcessBox {
    private String pathFile;
    WorkBookServices workBookServices;
    ExcelServices excelServices;

    public ProcessBox(String pathFile) {
        this.pathFile = pathFile;
        this.workBookServices = new WorkBookServices(pathFile);
        this.excelServices = new ExcelServices(workBookServices);
    }

    public void startProcess() {
        long startTime = System.currentTimeMillis();
        int totalRecords = 0;
        for (Row row : workBookServices.getWorkBookServices().getSheetAt(0)) {
            totalRecords++;
        }

        Semaphore semaphore = new Semaphore(50);
        CountDownLatch latch = new CountDownLatch(totalRecords);
        ArrayList<ErrorEntity> errorList = new ArrayList<>();

        for (Row row : workBookServices.getWorkBookServices().getSheetAt(0)) {
            UserEntity userEntity = excelServices.readUserByRow(row);
            System.out.println("Validate user: " + userEntity.getUserName());
            new ValidateThread(latch, semaphore, userEntity, errorList, pathFile).run();
        }

        try {
            latch.await();

            System.out.println();
            System.out.println("Validate finished! wait 5 seconds...");
            System.out.println();

            synchronized (this) {
                wait(5000);
            }

            FileInputStream fileInputStream = null;
            FileOutputStream fileOutputStream = null;
            try {
                fileInputStream = new FileInputStream(workBookServices.getPathFile());
                Workbook workbook = new HSSFWorkbook(fileInputStream);
                Sheet sheet = workbook.getSheetAt(0);

                for (ErrorEntity errorEntity : errorList) {
                    System.out.println("Write error for user: " + errorEntity.getUserName() + "/Error: " + errorEntity.getError());
                    for (Row row : sheet) {
                        if (row.getCell(1).getStringCellValue().equals(errorEntity.getUserName())) {
                            Cell cell = row.createCell(9);
                            cell.setCellValue(errorEntity.getError());
                        }
                    }
                }

                fileOutputStream = new FileOutputStream(workBookServices.getPathFile());
                workbook.write(fileOutputStream);
                fileOutputStream.close();
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println();
            long endTime = System.currentTimeMillis();
            System.out.println("Running time: " + (endTime-startTime) + "mS");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
