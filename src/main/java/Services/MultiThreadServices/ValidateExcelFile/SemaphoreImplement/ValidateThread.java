package Services.MultiThreadServices.ValidateExcelFile.SemaphoreImplement;

import Services.MultiThreadServices.ValidateExcelFile.Entities.ErrorEntity;
import Services.MultiThreadServices.ValidateExcelFile.Entities.UserEntity;
import Services.MultiThreadServices.ValidateExcelFile.ExcelServices;
import Services.MultiThreadServices.ValidateExcelFile.WorkBookServices;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class ValidateThread implements Runnable{
    private final CountDownLatch latch;
    private final Semaphore semaphore;
    private final UserEntity userEntity;
    private final ArrayList<ErrorEntity> errorList;
    private String pathFile;
    private WorkBookServices workBookServices;
    private ExcelServices excelServices;

    public ValidateThread(CountDownLatch latch, Semaphore semaphore, UserEntity userEntity, ArrayList<ErrorEntity> errorList, String pathFile) {
        this.latch = latch;
        this.semaphore = semaphore;
        this.userEntity = userEntity;
        this.errorList = errorList;
        this.pathFile = pathFile;
        this.workBookServices = new WorkBookServices(pathFile);
        this.excelServices = new ExcelServices(workBookServices);
    }

    @Override
    public void run() {
        try {
            try {
                semaphore.acquire();
                String error = "" + excelServices.validateUserLocation(userEntity);
                errorList.add(new ErrorEntity(userEntity.getUserName(), error));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } finally {
            latch.countDown();
            semaphore.release();
        }
    }
}
