package Services.MultiThreadServices.ValidateExcelFile.Entities;

public class UserEntity {
    private String userName;
    private int genderId;
    private int province_cityId;
    private int district_townId;
    private int commune_wardId;
    private int religionId;
    private int ethnicId;
    private String md5hash;

    public UserEntity() {
    }

    public UserEntity(String userName, int genderId, int province_cityId, int district_townId, int commune_wardId, int religionId, int ethnicId, String md5hash) {
        this.userName = userName;
        this.genderId = genderId;
        this.province_cityId = province_cityId;
        this.district_townId = district_townId;
        this.commune_wardId = commune_wardId;
        this.religionId = religionId;
        this.ethnicId = ethnicId;
        this.md5hash = md5hash;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getGenderId() {
        return genderId;
    }

    public void setGenderId(int genderId) {
        this.genderId = genderId;
    }

    public int getProvince_cityId() {
        return province_cityId;
    }

    public void setProvince_cityId(int province_cityId) {
        this.province_cityId = province_cityId;
    }

    public int getDistrict_townId() {
        return district_townId;
    }

    public void setDistrict_townId(int district_townId) {
        this.district_townId = district_townId;
    }

    public int getCommune_wardId() {
        return commune_wardId;
    }

    public void setCommune_wardId(int commune_wardId) {
        this.commune_wardId = commune_wardId;
    }

    public int getReligionId() {
        return religionId;
    }

    public void setReligionId(int religionId) {
        this.religionId = religionId;
    }

    public int getEthnicId() {
        return ethnicId;
    }

    public void setEthnicId(int ethnicId) {
        this.ethnicId = ethnicId;
    }

    public String getMd5hash() {
        return md5hash;
    }

    public void setMd5hash(String md5hash) {
        this.md5hash = md5hash;
    }
}
