package Services.MultiThreadServices.ValidateExcelFile.Entities;

public class ErrorEntity {
    private String userName;
    private String error;

    public ErrorEntity(String userName, String error) {
        this.userName = userName;
        this.error = error;
    }

    public ErrorEntity() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
