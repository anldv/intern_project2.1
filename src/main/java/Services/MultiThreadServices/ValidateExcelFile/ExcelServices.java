package Services.MultiThreadServices.ValidateExcelFile;

import Services.MultiThreadServices.ValidateExcelFile.Entities.ErrorEntity;
import Services.MultiThreadServices.ValidateExcelFile.Entities.UserEntity;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ExcelServices{
    private WorkBookServices workBookServices;

    public ExcelServices(WorkBookServices workBookServices) {
        this.workBookServices = workBookServices;
    }

    // Thread getList:
    // loop to read list
    public List<UserEntity> getUserList() {
        List<UserEntity> userEntityList = new ArrayList<>();
        int i = 1;
        for (Row row : workBookServices.getWorkBookServices().getSheetAt(0)){
            UserEntity thisUserEntity = readUserByRowNumber(i);
            System.out.println("Get user by row number: " + (i+1) + ". Name: " + thisUserEntity.getUserName());
            userEntityList.add(thisUserEntity);
            i++;
        }
        return userEntityList;
    }

    //Read row:
    // input: int row number
    // output: user
    public UserEntity readUserByRowNumber(int rowNumber) {
        Sheet sheet = workBookServices.getWorkBookServices().getSheetAt(0);
        Row row=sheet.getRow(rowNumber);
        String userName = row.getCell(1).getStringCellValue();
        int genderId = (int) row.getCell(2).getNumericCellValue();
        int province_cityId = (int) row.getCell(3).getNumericCellValue();
        int district_townId = (int) row.getCell(4).getNumericCellValue();
        int commune_wardId = (int) row.getCell(5).getNumericCellValue();
        int religionId = (int) row.getCell(6).getNumericCellValue();
        int ethnicId = (int) row.getCell(7).getNumericCellValue();
        String md5hash = row.getCell(8).getStringCellValue();
        UserEntity userEntity = new UserEntity(userName, genderId, province_cityId, district_townId, commune_wardId, religionId, ethnicId, md5hash);
        return userEntity;
    }

    public UserEntity readUserByRow(Row row) {
        String userName = row.getCell(1).getStringCellValue();
        int genderId = (int) row.getCell(2).getNumericCellValue();
        int province_cityId = (int) row.getCell(3).getNumericCellValue();
        int district_townId = (int) row.getCell(4).getNumericCellValue();
        int commune_wardId = (int) row.getCell(5).getNumericCellValue();
        int religionId = (int) row.getCell(6).getNumericCellValue();
        int ethnicId = (int) row.getCell(7).getNumericCellValue();
        String md5hash = row.getCell(8).getStringCellValue();
        return new UserEntity(userName, genderId, province_cityId, district_townId, commune_wardId,
                religionId, ethnicId, md5hash);
    }

    //Validate user
    // input: user
    // output: error, error index
    public String validateUserLocation(UserEntity inputUserEntity) {
        String error = "";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/market?characterEncoding=utf8", "guess", "guess123password");
            Statement statement = connection.createStatement();
            String sql = "SELECT distinct * FROM market.administrative_decentralized where commune_wardId = '" + inputUserEntity.getCommune_wardId() + "';";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                int commune_wardId_db = resultSet.getInt(6);
                String commune_wardName_db = resultSet.getString(5);
                int district_townId_db = resultSet.getInt(4);
                String district_townName_db = resultSet.getString(3);
                int province_cityId_db = resultSet.getInt(2);
                String province_cityName_db = resultSet.getString(1);
                if (district_townId_db != inputUserEntity.getDistrict_townId()) {
                    error = error + ("Lỗi địa chỉ: " + commune_wardName_db + " phải thuộc " + district_townName_db + ". ");
                }
                if (province_cityId_db != inputUserEntity.getProvince_cityId()) {
                    error = error + ("Lỗi địa chỉ: " + district_townName_db + " phải thuộc " + province_cityName_db + ". ");
                }
            }
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Error, see output console: " + e);
        }
        return error;
    }

    //Write error
    //Input ErrorEntity
    //Output boolean success?
    public synchronized void writeError(ErrorEntity errorEntity) {
        try {
            FileInputStream fileInputStream = new FileInputStream(workBookServices.getPathFile());
            Workbook workbook = new HSSFWorkbook(fileInputStream);
            Sheet sheet = workbook.getSheetAt(0);
            for (Row row : sheet) {
                if (row.getCell(1).getStringCellValue().equals(errorEntity.getUserName())) {
                    Cell cell = row.createCell(9);
                    cell.setCellValue(errorEntity.getError());
                }
            }
            FileOutputStream fileOutputStream = new FileOutputStream(workBookServices.getPathFile());
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void writeExample(){
        System.out.println("Start write");
        try {
            FileInputStream fileInputStream = new FileInputStream(workBookServices.getPathFile());
            Workbook workbook = new HSSFWorkbook(fileInputStream);
            Sheet sheet = workbook.getSheetAt(0);
            Cell cell = sheet.getRow(1).createCell(9);
            cell.setCellValue("None");
            FileOutputStream fileOutputStream = new FileOutputStream(workBookServices.getPathFile());
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Done");
    }

    public static void main(String[] args) {
        WorkBookServices workBookServices = new WorkBookServices("D:\\Services\\WorkingOn\\Edsolabs\\Report.1\\test1.xls");
        ExcelServices excelServices = new ExcelServices(workBookServices);
        //UserEntity userEntity = excelServices.readUserByRowNumber(2);
        //System.out.println("Name: " + userEntity.getUserName());
        //System.out.println();
        //System.out.println(excelServices.validateUserLocation(excelServices.readUserByRowNumber(1)));
        //List<UserEntity> list = excelServices.getUserList();
        excelServices.writeExample();
    }
}
