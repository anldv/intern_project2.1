package Services.MultiThreadServices.ValidateExcelFile.ThreadpoolImplement;

import Services.MultiThreadServices.ValidateExcelFile.Entities.ErrorEntity;
import Services.MultiThreadServices.ValidateExcelFile.Entities.UserEntity;
import Services.MultiThreadServices.ValidateExcelFile.ExcelServices;
import Services.MultiThreadServices.ValidateExcelFile.WorkBookServices;

public class ValidateThread implements Runnable {
    private UserEntity userEntity;
    private String pathFile;
    private WorkBookServices workBookServices;
    private ExcelServices excelServices;

    @Override
    public void run() {
        String error = excelServices.validateUserLocation(userEntity);
        ErrorEntity errorEntity = new ErrorEntity(userEntity.getUserName(), error);
        //put task to queue
    }

    public ValidateThread(UserEntity userEntity, String pathFile) {
        this.pathFile = pathFile;
        this.workBookServices = new WorkBookServices(pathFile);
        this.excelServices = new ExcelServices(workBookServices);
        this.userEntity = userEntity;
    }
}
