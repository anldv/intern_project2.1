package Services.MultiThreadServices.ValidateExcelFile.ThreadpoolImplement;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolServices1 {
    private int maxPoolSize;
    private ArrayList<ValidateThread> threadList;

    public ThreadPoolServices1(int maxPoolSize, ArrayList<ValidateThread> threadList) {
        ExecutorService pool = Executors.newFixedThreadPool(maxPoolSize);
        this.threadList = threadList;
        for (int i = 0; i < threadList.size(); i++) {
            pool.submit(threadList.get(i));
        }
        pool.shutdown();
    }
}
