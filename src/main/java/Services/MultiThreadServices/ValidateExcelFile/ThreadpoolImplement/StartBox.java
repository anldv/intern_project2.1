package Services.MultiThreadServices.ValidateExcelFile.ThreadpoolImplement;

import Services.MultiThreadServices.ValidateExcelFile.Entities.UserEntity;
import Services.MultiThreadServices.ValidateExcelFile.ExcelServices;
import Services.MultiThreadServices.ValidateExcelFile.WorkBookServices;
import org.apache.poi.ss.usermodel.Row;

import java.util.ArrayList;
import java.util.List;

public class StartBox {
    private String pathFile;
    private WorkBookServices workBookServices;
    private ExcelServices excelServices;

    public StartBox (String pathFile) {
        this.pathFile = pathFile;
        this.workBookServices = new WorkBookServices(pathFile);
        this.excelServices = new ExcelServices(workBookServices);
    }

    public void startProcess() {
        List<ValidateThread> threadList = new ArrayList<>();
        for (Row row : workBookServices.getWorkBookServices().getSheetAt(0)) {
            UserEntity userEntity = excelServices.readUserByRow(row);
            threadList.add(new ValidateThread(userEntity, pathFile));
        }
    }
}
