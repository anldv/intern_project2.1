package Services.MultiThreadServices.ValidateExcelFile;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class WorkBookServices {
    private String pathFile;

    public String getPathFile() {
        return this.pathFile;
    }

    public WorkBookServices(String pathFile) {
        this.pathFile = pathFile;
    }

    public Workbook getWorkBookServices() {
        Workbook workbook = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(pathFile);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

            if (pathFile.endsWith(".xlsx")) {
                workbook = new XSSFWorkbook(bufferedInputStream);
            } else if (pathFile.endsWith(".xls")) {
                workbook = new HSSFWorkbook(bufferedInputStream);
            } else throw new IllegalArgumentException("Not illegal input file");
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
        return workbook;
    }
}
